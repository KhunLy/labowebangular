import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Product} from '../_models/product';
import {Storage} from '@ionic/storage';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
    private products: Array<Product> = [];
    constructor(private httpClient: HttpClient, private storage: Storage) {}

    ngOnInit(): void {

        this.httpClient.get<Array<Product>>(environment.api_url + 'product')
            .subscribe((data) => {
                console.log(data);
                this.products = data;
            }, (error) => {
                console.log(error);
            });

    }

    addToBasket(prod: Product) {
        this.httpClient.post(environment.api_url + 'basket/' + prod.id, {})
            .subscribe((data) => {
                console.log(data);
            }, (error) => {
                console.log(error);
            });
    }
}
