import {AfterViewInit, Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Product} from '../_models/product';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.page.html',
  styleUrls: ['./panier.page.scss'],
})
export class PanierPage implements AfterViewInit {

  private basket: Array<Product> = [];
  constructor(
      private httpClient: HttpClient
  ) { }

  ngAfterViewInit() {
    this.httpClient.get<Array<Product>>(environment.api_url + 'basket')
        .subscribe(data => {
          this.basket = data;
        }, error => {
          console.log(error);
        });
  }

  remove(p: Product) {
    this.httpClient.delete(environment.api_url + 'basket/' + p.id).subscribe(data => {
      const index = this.basket.indexOf(p);
      this.basket.splice(index, 1);
    });
  }

}
