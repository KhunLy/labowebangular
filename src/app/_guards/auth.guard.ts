import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {getAngularClassTransformerFactory} from '@angular/compiler-cli/src/transformers/r3_transform';
import {Storage} from '@ionic/storage';
import {Injectable} from '@angular/core';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private storage: Storage,
        private router: Router
    ) {

    }
    async canActivate(route: ActivatedRouteSnapshot): Promise<boolean> {
        const token = await this.storage.get('token');
        let tokenIsValid = true;
        if (token != null) {
            tokenIsValid
                = this.parseJwt(token).exp * 1000 > (new Date()).getTime();
            console.log(this.parseJwt(token));
        }
        if (token == null || !tokenIsValid) {
            // redirection
            this.router.navigateByUrl('login');
            return false;
        }
        return true;
    }

    private parseJwt(token) {
        const base64Url = token.split('.')[1];
        const base64 = decodeURIComponent(atob(base64Url).split('').map((c) => {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));

        return JSON.parse(base64);
    }
}
