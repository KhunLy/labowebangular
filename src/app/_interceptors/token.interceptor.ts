import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {from, Observable} from 'rxjs';
import {Storage} from '@ionic/storage';
import {Injectable} from '@angular/core';
import {switchMap} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class TokenInterceptor implements HttpInterceptor {
    constructor(
        private storage: Storage
    ) {}
    intercept(req: HttpRequest<any>, next: HttpHandler)
        : Observable<HttpEvent<any>> {
        return from(this.storage.get('token'))
            .pipe(switchMap(token => {
            const headers = new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('authorization', 'Bearer ' + token);
            req = req.clone({headers: headers});
            return next.handle(req);
        }));
        // angular
        // const token = localStorage.get('token');
        // const headers = new HttpHeaders()
        //     .set('Content-Type', 'application/json')
        //     .set('authorization', 'Bearer ' + token);
        // req = req.clone({headers: headers});
        // return next.handle(req);


    }
}
