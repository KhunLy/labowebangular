import { Component, OnInit } from '@angular/core';
import {Login} from '../_models/login';
import {FormControl, FormGroup, Validator, Validators} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {AlertController, LoadingController, ToastController} from '@ionic/angular';
import {LoginResponse} from '../_models/login.response';
import {Storage} from '@ionic/storage';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private fd;

  constructor(
      private httpClient: HttpClient,
      private toastController: ToastController,
      private storage: Storage,
      private router: Router,
      private loadingCtrl: LoadingController
  ) { }

  ngOnInit() {
    this.fd = new FormGroup({
      username: new FormControl('', Validators.compose([
          Validators.required,
          Validators.email,
      ])),
      password: new FormControl('', Validators.compose([
          Validators.required,
          Validators.minLength(4)
      ]))
    });
  }

  async loginAction(login: Login) {
      const loader = await this.loadingCtrl.create({ message: 'Please Wait ...' });
      loader.present();
      this.httpClient.post<LoginResponse>(environment.api_url + 'login_check', login)
        .subscribe(data => {
            loader.dismiss();
            this.storage.set('token', data.token).then(() => {
                this.router.navigateByUrl('');
            });
        }, error => {
            loader.dismiss();
            this.toastController.create({
              header: 'Error',
              message: 'Bad Credentials',
              duration: 5000
          }).then(toast => {
              toast.present();
          });
        });
  }

}
